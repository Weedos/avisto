beautifulsoup4==4.8.0
colorlog==4.0.2
configparser==3.8.1
Delorean==1.0.0
pytest==5.1.1
requests==2.22.0
workalendar==6.0.0
