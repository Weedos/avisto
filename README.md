# What is this repository for?

Purpose of this repo is to store and share misc scripts related to AViSTO.
For now, it only contains a unique script used to fill the monthly activity report.

## Install

### With Docker

```sh
docker-compose run --rm avisto portail_avisto.py # your arguments here
# or, if you prefer:
source .bashrc
portail_avisto # your arguments here
```

Any change requires a rebuild with `docker-compose build`.

### Without Docker

You first need to install pip3 and some libs:

```sh
sudo apt-get install python3-pip python3-dev # have pip and build deps
pip3 install -U pip # optional: ensure latest version of pip
pip3 install setuptools # probably you don't need this
pip3 install -Ur requirements.txt
```

The -U ensure latest version of libraries.

### Create the secret file

In the same directory as the portail_avisto.py script, create a file `.secret`, following the example given in `.secret.example`.

## Examples

For all commands, 2 arguments can to be specified:

* -m: Month number, defaults to current month.
* -y: year number, defaults to current year.

### List the available tasks

```sh
python3 portail_avisto.py -m 7 -y 2017 --tasklist
```

> **A000000455     05**:Example of project 1
> A000000456     05:Example of project 2
> A000000500     00:INTERCONTRAT NICE
> A000000506     00:ABSENCES NICE
> A000000518     00:FORMATION NICE

### Fill a complete month

_Note: If an activity is already present for a day, it won't be replaced. Use the --overwrite argument to overwrite._

Arguments:

* -t: Task id from --tasklist, or something matching the name of the project (will fail if matching more than one)
* -w: Freetext, description of what you did

```sh
python3 portail_avisto.py -m 7 -y 2017 -t "Your task id goes here" -w "Your work goes here"
```

Based on the task list we received in the previous example:

```sh
python3 portail_avisto.py -m 7 -y 2017 -t "A000000455     05" -w "Automatic tests"
```

> INFO:__main__:Hello <your username>
> INFO:__main__:Is not working day: (2017, 7, 1)
> ...
> INFO:__main__:(2017, 7, 28) has activity
> INFO:__main__:Is not working day: (2017, 7, 29)
> INFO:__main__:Is not working day: (2017, 7, 30)
> INFO:__main__:Updating (2017, 7, 31)
> INFO:__main__:Result: {"success":true,"message":"Activité mise à jour avec succès"}

### Fill specified days

_Note: This will implicitly set the --overwrite argument._

#### Whole days (in this case: 1st and 2nd of the given month)

```sh
python3 portail_avisto.py -m 7 -y 2017 -d "1,2" -t "A000000455     05" -w "Automatic tests"
```

#### Partial days

Note: in this case, the whole day must already have an activity because the API doesn't allow to set only morning or afternoon.

```sh
python3 portail_avisto.py -m 7 -y 2017 -d "1AM,2PM" -t "A000000455     05" -w "Automatic tests"
```

### Get the summary of a month

_Note: It also returns a day-by-day report._

```sh
python3 portail_avisto.py -m 7 -y 2017 --summary
```

> ABS = 0.5
> FERIE = 1.0
> Example of project 1  = 18.5

### To close a month

```sh
python3 portail_avisto.py -m 7 -y 2017 --cloture
```
