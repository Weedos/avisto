FROM python:3-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN \
  apk --no-cache add \
    gcc \
    musl-dev \
  && pip install --no-cache-dir -r requirements.txt

COPY . .

USER guest

RUN pytest

ENTRYPOINT [ "python" ]
