from portail_avisto import parse_days
import pytest


def test_parse_days_valid():
    days = "1,2, 3 ,4"
    assert parse_days(days) == {
        1: ["AM", "PM"],
        2: ["AM", "PM"],
        3: ["AM", "PM"],
        4: ["AM", "PM"]
    }


def test_parse_days_with_pm_apm_valid():
    days = "1am,2, 3 ,4"
    assert parse_days(days) == {
        1: ["AM"],
        2: ["AM", "PM"],
        3: ["AM", "PM"],
        4: ["AM", "PM"]
    }


def test_invalid_value():
    with pytest.raises(ValueError):
        days = "1ap,2, 3 ,4"
        assert parse_days(days) == True
