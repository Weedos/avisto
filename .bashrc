#!/bin/bash

: "${AVISTO_ROOT:="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"}"

# shellcheck disable=SC2139
alias portail_avisto="docker-compose -f '$AVISTO_ROOT/docker-compose.yml' run --rm avisto portail_avisto.py"

unset AVISTO_ROOT
